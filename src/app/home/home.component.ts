import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service";
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products: any;
  productSrc:any;
  fruit:boolean = false;
  bakery:boolean = false;
  isAvailable:boolean = true;
  notAvailable:boolean = false;
  userSearchUpdate = new Subject<string>();
  userSearch:string;
  constructor(
    private dataService: DataService,
    private router: Router
  ) { 
    // Debounce search.
    this.userSearchUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        this.filterData(value)
      });
      this.dataService.productObservable.subscribe((data)=>{
        this.productSrc = data;
        this.products = data;
        console.log(this.products);
      });
  }

  ngOnInit(): void {
    console.log('=== ')
    this.getList();
  }

  getList(){
    
  }
  filterData(search ?: string){
    console.log(search);
    // var filtered = [];
    console.log(this.fruit, this.bakery);
    var filtered = this.productSrc.filter(el=>{
      if(this.fruit){
        return el.type == 'fruit';
      }
      if(this.bakery){
        return el.type == 'bakery';
      }
      if(search){
        return el.title.toLowerCase().includes(search.toLowerCase());
      }
      if(!this.fruit && !this.bakery && !search){
        return el;
      }
    })
    this.products = filtered;
    console.log(this.products.length)
  }
  moveToTrash(i){
    let c = confirm('Are You sure you want to move it to trash!??')
    if(c){
      this.products[i].available = false;
      console.log(this.products)
    }
  }
  isavailable(){
    //avl
  }
  addNew(){
    this.router.navigate(['/add']);
  }

}
