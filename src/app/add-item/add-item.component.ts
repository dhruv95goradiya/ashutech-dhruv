import { Component, OnInit } from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
  FormGroupDirective
} from "@angular/forms";
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  itemForm: FormGroup;
  titleCtrl = new FormControl("", [Validators.required]);
  typeCtrl = new FormControl("", [Validators.required]);
  descCtrl = new FormControl("", [Validators.required]);
  priceCtrl = new FormControl(0, [Validators.required]);
  products: any;
  reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
  ngOnInit(): void {
  }
  constructor(private dataService: DataService,
    private fb: FormBuilder,
    public snk: MatSnackBar,
    private router: Router) {

      this.itemForm = this.fb.group({
        titleCtrl: ["", Validators.compose([Validators.required])],
        typeCtrl: ["", Validators.compose([Validators.required])],
        descCtrl: ["", Validators.compose([Validators.required])],
        priceCtrl: [0, Validators.compose([Validators.required])],
        urlCtrl: ['', [Validators.required, Validators.pattern(this.reg)]]
      })
    // this.itemForm = new FormGroup({
    //   titleCtrl: this.titleCtrl,
    //   typeCtrl: this.typeCtrl,
    //   descCtrl: this.descCtrl,
    //   priceCtrl: this.priceCtrl,
    // });
    this.products = this.dataService.getList();
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.itemForm.controls[controlName].hasError(errorName);
  };
  save(formDirective: FormGroupDirective){
    console.log('Save Action ', this.itemForm.value);
    // alert();
    let fdata = this.itemForm.value;
    
    let tmp = {
      "title": this.itemForm.value['titleCtrl'],
        "type": this.itemForm.value['typeCtrl'],
        "description": this.itemForm.value['descCtrl'],
        "filename": this.itemForm.value['urlCtrl'],
        "height": 450,
        "width": 299,
        "price": this.itemForm.value['priceCtrl'],
        "rating": 2,
        "available": true
    }
    this.products = [tmp].concat(this.products);
    this.itemForm.reset();
    formDirective.resetForm();
    this.snk.open('Item added!',"Ok", <any>{
      duration: 3000,
    });
    console.log(this.products);
  }


}
