import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from "../../services/data.service";

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  pindex: any;
  products: any;
  product: any;

  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router) { 
    //! get products
    this.products = this.dataService.getList();
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        // console.log(params); 
        //! check query param index
        if(params.index){
          this.pindex = params.index;
          this.product = this.products[this.pindex]
        }
      }
    );
  }

  ngAfterViewInit(): void {
    if(!this.pindex || !this.product){
      //! go home if no details
      this.router.navigate(['/']);
    }
  }

}
