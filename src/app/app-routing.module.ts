import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router'; 
import { HomeComponent } from './home/home.component'; 
import { ItemDetailComponent } from "./item-detail/item-detail.component";
import { TrashComponent } from "./trash/trash.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'detail', component: ItemDetailComponent },
  { path: 'trash', component: TrashComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full'}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export const RoutingComponent = [HomeComponent,ItemDetailComponent,TrashComponent ];
export class AppRoutingModule { }


