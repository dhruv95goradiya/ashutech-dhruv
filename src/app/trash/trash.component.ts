import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service";
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-trash',
  templateUrl: './trash.component.html',
  styleUrls: ['./trash.component.css']
})
export class TrashComponent implements OnInit {

  products: any;
  productSrc:any;
  fruit:boolean = false;
  bakery:boolean = false;
  userSearchUpdate = new Subject<string>();
  userSearch:string;
  count:number=0;
  constructor(
    private dataService: DataService
  ) { 
    
  }

  ngOnInit(): void {
    console.log('=== ')
    this.getList();
  }

  getList(){
    this.count = 0;
    this.products = this.dataService.getList();
    this.productSrc = this.products;
    console.log(this.products);
    this.productSrc.forEach((el)=>{
      if(el.available == false){
        this.count++;
      }
    })
  }
  restore(i){
    this.products[i].available = true;
    this.count--;
    console.log(this.products)
  }
  remove(i){
    this.products.splice(i,1);
  }



}
