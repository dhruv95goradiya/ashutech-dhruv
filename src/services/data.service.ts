import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
// import * as data  from "../data/products.json";

declare var require: any;
export interface Item {
  title: any;
  type: any;
  description: any;
  filename: any;
  height: any;
  width: any;
  price: any;
  rating: any;
  available: any;
}
var products =  require("../data/products.json");

@Injectable({
  providedIn: 'root'
})
export class DataService {
  srcProducts: Item = null;
  private currentProductSource = new BehaviorSubject<any>(null);
  productObservable = this.currentProductSource.asObservable();
  public _jsonURL = './data/products.json';
  // products: any = (data as any).default;
  constructor(private http: HttpClient) { 
    this.getList();
  }
  public getList() {
    this.srcProducts = products;
    this.currentProductSource.next(products);
    return products;
  }
}
